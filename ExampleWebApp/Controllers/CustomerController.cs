﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ExampleWebApp.Controllers
{
    public class Customer
    {
        public string Name { get; set; }
        public string SurName { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class CustomerController : Controller
    {
        public ActionResult Index()
        {
            //1. stworzymy liste obiektów typu Customer
            //2. Wypełnimy listę danymi
            var customers = new List<Customer>()
            {
                new Customer { Name = "Marek", SurName = "Oniszczuk", PhoneNumber = "666666666" },
                new Customer { Name = "Jarek", SurName = "Niszczuk", PhoneNumber = "8888888888" },
                new Customer { Name = "Darek", SurName = "Iszczuk", PhoneNumber = "67666666"},
            };


            ViewBag.number = 10;
//            ViewData

            //3. Przekażemy listę jako parametr do metody View()
            return View(customers);

            //4. Modyfikacja pliku Index.cshtml - wyświetlenie listy za pomocą pętli foreach.
        }
    }
}
